//
//  Note.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+AES256.h"

@interface Note : NSObject<NSCoding>{
    NSData *_titleEncrypted;
    NSData *_textDataEncrypted;
}



-(void)setText:(NSString *)text withKey:(NSString *)key;
-(NSString *)textFromKey:(NSString *)key;
-(void)setTitle:(NSString *)text withKey:(NSString *)key;
-(NSString *)titleFromKey:(NSString *)key;
-(id)initWithKey:(NSString *)key;
@end
