//
//  NotesModel.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "NotesModel.h"

@implementation NotesModel
@synthesize passwordHash = _passwordHash, notesArray = _notesArray, key = _key;
-(id)init{
    self = [super init];
    if (self){
        _notesArray = [[NSMutableArray alloc] init];
        NSString *tempPassword = [[NSString alloc] initWithFormat:@"hello"];
        self.passwordHash = [tempPassword sha512];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    NSLog(@"initWithCoder called for notesModel");
    self = [super init];
    if (self) {
        _notesArray = [aDecoder decodeObjectForKey:@"notesArray"];
        _passwordHash = [aDecoder decodeObjectForKey:@"passwordHash"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    NSLog(@"encodeWithCoder called for NotesModel");
    [aCoder encodeObject:_notesArray forKey:@"notesArray"];
    [aCoder encodeObject:_passwordHash forKey:@"passwordHash"];
}
@end
