//
//  AppDelegate.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthenticationViewController.h"
#import "SetPasswordViewController.h"
#import "ListViewController.h"
#import "NotesModel.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UINavigationController *_navController;
    AuthenticationViewController *_authViewController;
    UIWindow *_window;
    NotesModel *_notesModel;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) UINavigationController *navController;
@property (nonatomic) NotesModel *notesModel;
@property (nonatomic) AuthenticationViewController *authViewController;
@end
