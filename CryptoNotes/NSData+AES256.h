//
//  NSData+AES256.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;
@end
