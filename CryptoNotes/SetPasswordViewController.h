//
//  SetPasswordViewController.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesModel.h"
#import "Note.h"
@interface SetPasswordViewController : UIViewController <UITextFieldDelegate>{
    IBOutlet UITextField *oldPasswordField;
    IBOutlet UITextField *newPasswordFieldFirst;
    IBOutlet UITextField *newPasswordFieldSecond;
    IBOutlet UILabel *wrongLabel;
    IBOutlet UIButton *changeButton;
    NotesModel *_notesModel;
    IBOutlet UILabel *matchErrorLabel;
}

-(IBAction)change:(id)sender;

@property (nonatomic) NotesModel *notesModel;
@end
