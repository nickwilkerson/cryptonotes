//
//  ListViewController.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "ListViewController.h"

@interface ListViewController ()

@end

@implementation ListViewController
@synthesize notesModel = _notesModel;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        editing = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableArray *toolButtons = [[NSMutableArray alloc] initWithCapacity:1];
    UIBarButtonItem *addNoteButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addNote)];
    UIBarButtonItem *editNotesButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(edit)];
    UIBarButtonItem *changePassword = [[UIBarButtonItem alloc] initWithTitle:@"Change Password" style:UIBarButtonItemStylePlain target:self action:@selector(changePassword)];
    [toolButtons addObject:changePassword];
    [self setToolbarItems:toolButtons animated:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlack];
    [self.navigationController.toolbar setTranslucent:YES];
    [self.navigationController setToolbarHidden:NO];
    
    self.navigationItem.rightBarButtonItem = addNoteButton;
    self.navigationItem.leftBarButtonItem = editNotesButton;
    [self.tableView setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)changePassword{
    SetPasswordViewController *setPasswordVC;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        setPasswordVC = [[SetPasswordViewController alloc] initWithNibName:@"SetPasswordViewController-iPad" bundle:nil];
    } else {
        setPasswordVC = [[SetPasswordViewController alloc] initWithNibName:@"SetPasswordViewController" bundle:nil];
    }
    setPasswordVC.notesModel = self.notesModel;
    [self.navigationController pushViewController:setPasswordVC animated:YES];
}

-(void)edit{
    if (editing) {
        [self.tableView setEditing:NO animated:YES];
        editing = NO;
    }
    else {
        [self.tableView setEditing:YES animated:YES];
        editing = YES;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
}

-(void)addNote{
    NSLog(@"Adding a note");
    Note *note = [[Note alloc] initWithKey:self.notesModel.key];
    NSLog(@"Adding note to notesArray");
    [self.notesModel.notesArray addObject:note];
    NSLog(@"creating noteViewController");
    NoteViewController *detailViewController;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        detailViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController-iPad" bundle:nil note:note notesModel:self.notesModel];
    } else {
        detailViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController" bundle:nil note:note notesModel:self.notesModel];
    }
    NSLog(@"setting notesModel for noteViewController");
    [detailViewController setNotesModel:self.notesModel];
    NSLog(@"setting note for noteViewController");
    [detailViewController setNote:note];
    NSLog(@"pushing ViewController detailViewController");
    [self.navigationController pushViewController:detailViewController animated:YES];
    NSLog(@"successfully added note to notesArray and launched NoteViewController");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"numberOfRowsInSection called");
    return [self.notesModel.notesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath called");
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.textColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
    cell.textLabel.text = [[self.notesModel.notesArray objectAtIndex:indexPath.row] titleFromKey:self.notesModel.key];
    
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.notesModel.notesArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        NSLog(@"Adding a note");
        Note *note = [[Note alloc] initWithKey:self.notesModel.key];
        NSLog(@"Adding note to notesArray");
        [self.notesModel.notesArray addObject:note];
        //self.tableView
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    NoteViewController *detailViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        detailViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController-iPad" bundle:nil note:[self.notesModel.notesArray objectAtIndex:indexPath.row] notesModel:self.notesModel];
    } else {
        detailViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController" bundle:nil note:[self.notesModel.notesArray objectAtIndex:indexPath.row] notesModel:self.notesModel];
    }
    [detailViewController setNotesModel:self.notesModel];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     
}

@end
