//
//  Note.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "Note.h"

@implementation Note

-(id)initWithKey:(NSString *)key{
    self = [super init];
    if (self){
        [self setTitle:@"New Note" withKey:key];
        [self setText:@"New Note Data" withKey:key];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    //NSLog(@"initWithCoder called for Note");
    self = [super init];
    if (self) {
        _titleEncrypted = [aDecoder decodeObjectForKey:@"titleEncrypted"];
        _textDataEncrypted = [aDecoder decodeObjectForKey:@"textDataEncrypted"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_titleEncrypted forKey:@"titleEncrypted"];
    [aCoder encodeObject:_textDataEncrypted forKey:@"textDataEncrypted"];
}

-(void)setText:(NSString *)text withKey:(NSString *)key{
    //NSLog(@"encrypting text: %@", text);
    if ([key isEqualToString:@""]) {
        NSLog(@"No key not encrypting");
    } else {
        NSData *dataClear = [text dataUsingEncoding:NSUTF8StringEncoding];
        _textDataEncrypted = [dataClear AES256EncryptWithKey:key];
    }
}


-(NSString *)textFromKey:(NSString *)key{
   // NSLog(@"decrypting");
    NSData *decryptedData = [_textDataEncrypted AES256DecryptWithKey:key];
    NSString *decryptedText = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
    //NSLog(@"decrypted text: %@", decryptedText);
    return decryptedText;
}

-(void)setTitle:(NSString *)text withKey:(NSString *)key{
    NSLog(@"encrypting text: %@ withKey: %@", text, key);
    if ([key isEqualToString:@""]) {
        NSLog(@"No key not encrypting");
    } else {
        NSData *dataClear = [text dataUsingEncoding:NSUTF8StringEncoding];
        _titleEncrypted = [dataClear AES256EncryptWithKey:key];
    }
}


-(NSString *)titleFromKey:(NSString *)key{
  //  NSLog(@"decrypting");
    NSData *decryptedData = [_titleEncrypted AES256DecryptWithKey:key];
    NSString *decryptedText = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
  //  NSLog(@"decrypted text: %@", decryptedText);
    return decryptedText;
}

@end
