//
//  SetPasswordViewController.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SetPasswordViewController.h"

@interface SetPasswordViewController ()

@end

@implementation SetPasswordViewController
@synthesize notesModel= _notesModel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    oldPasswordField.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    newPasswordFieldFirst.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    newPasswordFieldSecond.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    changeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    changeButton.titleLabel.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    changeButton.titleLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidDisappear:(BOOL)animated{
    [oldPasswordField setText:@""];
    [newPasswordFieldFirst setText:@""];
    [newPasswordFieldSecond setText:@""];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == oldPasswordField) {
        [newPasswordFieldFirst becomeFirstResponder];
    } else if (textField == newPasswordFieldFirst) {
        [newPasswordFieldSecond becomeFirstResponder];
    } else if (textField == newPasswordFieldSecond) {
        [self changePassword];
    }
    return NO;
}

-(IBAction)change:(id)sender{
    [self changePassword];
}

-(void)changePassword{
    if (![[oldPasswordField.text sha512] isEqualToString:self.notesModel.passwordHash]) {
        [wrongLabel setText:@"Try Again"];
        [oldPasswordField setText:@""];
        [oldPasswordField becomeFirstResponder];
    } else if (![newPasswordFieldFirst.text isEqualToString:newPasswordFieldSecond.text]){
        [matchErrorLabel setText:@"Passwords don't Match"];
        [newPasswordFieldFirst setText:@""];
        [newPasswordFieldSecond setText:@""];
    } else {
        NSString *oldPassword = self.notesModel.key;
        NSString *newPassword = newPasswordFieldFirst.text;
        for(Note *note in self.notesModel.notesArray) {
            NSString *textString = [note textFromKey:oldPassword];
            NSString *titleString = [note titleFromKey:oldPassword];
            [note setText:textString withKey:newPassword];
            [note setTitle:titleString withKey:newPassword];
        }
        self.notesModel.key = newPassword;
        self.notesModel.passwordHash = [newPassword sha512];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
