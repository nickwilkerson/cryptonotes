//
//  ListViewController.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesModel.h"
#import "NoteViewController.h" 
#import "SetPasswordViewController.h"
@interface ListViewController : UITableViewController{
    NotesModel *_notesModel;
    BOOL editing;
}
@property (nonatomic) NotesModel *notesModel;
@end
