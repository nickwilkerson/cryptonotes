//
//  NoteViewController.h//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Note.h"
#import "NotesModel.h"

@interface NoteViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>{
    NotesModel *_notesModel;
    Note *_note;
    IBOutlet UITextField *titleField;
    IBOutlet UITextView *textView;
}

@property (nonatomic) Note *note;
@property (nonatomic) NotesModel *notesModel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil note:(Note *)note notesModel:(NotesModel *)nm;
-(void)storeData;
@end
