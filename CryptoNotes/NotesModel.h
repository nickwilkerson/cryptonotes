//
//  NotesModel.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+CryptoString.h"
@interface NotesModel : NSObject<NSCoding>{
    NSMutableArray *_notesArray;
    NSString *_passwordHash;
    NSString *_key;
}

@property (nonatomic, readonly) NSMutableArray *notesArray;
@property (nonatomic) NSString *passwordHash;
@property (nonatomic) NSString *key;
@end
