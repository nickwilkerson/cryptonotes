//
//  NSString+CryptoString.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CryptoString)
-(NSString *)sha1;
-(NSString *)sha256;
-(NSString *)sha512;
@end
