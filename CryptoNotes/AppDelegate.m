//
//  AppDelegate.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "AppDelegate.h"


@implementation AppDelegate
@synthesize navController = _navController, window = _window, notesModel = _notesModel, authViewController = _authViewController;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _notesModel = [NSKeyedUnarchiver unarchiveObjectWithFile:[self pathForDataFile]];
    if (!_notesModel) {
        _notesModel = [[NotesModel alloc] init];
    }
    self.authViewController = [[AuthenticationViewController alloc] initWithNibName:@"AuthenticationViewController" bundle:nil];
    [self.authViewController setNotesModel:self.notesModel];
	//mainViewController.listContent = listContent;
	ListViewController *listViewController = [[ListViewController alloc] initWithStyle:UITableViewStylePlain];
    [listViewController setNotesModel:self.notesModel];
	// Add create and configure the navigation controller.
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];

	self.navController = navigationController;
    self.navController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
	
	// Configure and display the window.*/
	[self.window addSubview:self.navController.view];
	[self.window makeKeyAndVisible];
    [self.navController pushViewController:self.authViewController animated:YES];
    
    return YES;
}



- (NSString *) pathForDataFile {
    NSArray *documentDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString*	path = nil;
 	
    if (documentDir) {
        path = [documentDir objectAtIndex:0];
    }
 	NSLog(@"path = %@/%@", path, @"SettingsData.data");
    return [NSString stringWithFormat:@"%@/%@", path, @"SettingsData.data"];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    if ([[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] isKindOfClass:[NoteViewController class]]) {
        NSLog(@"killing app with noteViewController on top");
        [[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] storeData];
        [self.navController popToRootViewControllerAnimated:NO];
    } else if ([[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] isKindOfClass:[AuthenticationViewController class]]) {
        [self.navController popToRootViewControllerAnimated:NO];
    } else if ([[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] isKindOfClass:[SetPasswordViewController class]]){
        [self.navController popToRootViewControllerAnimated:NO];
    }
    
    [NSKeyedArchiver archiveRootObject:_notesModel toFile:[self pathForDataFile]];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"applicationDidEnterBackground");
 /*       if ([[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] isKindOfClass:[NoteViewController class]]) {
        NSLog(@"killing app with noteViewController on top");
        [[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] storeData];
        [self.navController popToRootViewControllerAnimated:NO];
    } else if ([[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] isKindOfClass:[AuthenticationViewController class]]) {
        [self.navController popToRootViewControllerAnimated:NO];
    } else if ([[[self.navController viewControllers] objectAtIndex:([self.navController.viewControllers count]-1)] isKindOfClass:[SetPasswordViewController class]]){
        [self.navController popToRootViewControllerAnimated:NO];
    }*/
    self.notesModel.key = @"";
 //   [NSKeyedArchiver archiveRootObject:_notesModel toFile:[self pathForDataFile]];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self.navController pushViewController:self.authViewController animated:NO];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
