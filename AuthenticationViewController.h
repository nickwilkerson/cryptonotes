//
//  AuthenticationViewController.h
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesModel.h"
#import "NSString+CryptoString.h"
@interface AuthenticationViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField *passwordField;
    IBOutlet UILabel *wrongLabel;
    NotesModel *_notesModel;
}

@property (nonatomic) NotesModel *notesModel;
@end
