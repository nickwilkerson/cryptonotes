//
//  NoteViewController.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "NoteViewController.h"

@interface NoteViewController ()

@end

@implementation NoteViewController
@synthesize note = _note, notesModel = _notesModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil note:(Note *)note notesModel:(NotesModel *)nm
{
    NSLog(@"------initializing NoteViewController------");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.notesModel = nm;
        self.note = note;
        [textView setDelegate:self];
        [titleField setDelegate:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[titleField setText:[self.note titleFromKey:self.notesModel.key]];
    [titleField setBackgroundColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1]];
    [titleField setTextColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
    [textView setText:[self.note textFromKey:self.notesModel.key]];
    [textView setTextColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
    [textView setBackgroundColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1]];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
}

-(void)viewDidAppear:(BOOL)animated{
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {

        [textField resignFirstResponder];
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    NSLog(@"viewWillDisappear called for NoteViewController");
    [self storeData];
    [[[self.navigationController.viewControllers objectAtIndex:0] tableView] reloadData];
}

-(void)storeData
{
    NSLog(@"NoteViewController storingData");
    [self.note setText:textView.text withKey:self.notesModel.key];
    [self.note setTitle:titleField.text withKey:self.notesModel.key];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    NSLog(@"NoteViewController viewDidDisappear");
//[self storeData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
