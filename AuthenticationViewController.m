//
//  AuthenticationViewController.m
//  CryptoNotes
//
//  Created by Nicholas Wilkerson on 3/28/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "AuthenticationViewController.h"

@interface AuthenticationViewController ()

@end

@implementation AuthenticationViewController
@synthesize notesModel = _notesModel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationItem.hidesBackButton = YES;
    }
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == passwordField) {
        if ([[passwordField.text sha512] isEqualToString:self.notesModel.passwordHash]) {
            [wrongLabel setText:@""];
            [self.notesModel setKey:passwordField.text];
            [[[self.navigationController.viewControllers objectAtIndex:0] tableView] reloadData];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [passwordField setText:@""];
        } else {
            [wrongLabel setText:@"Try Again"];
            wrongLabel.textColor = [UIColor colorWithRed:1.0 green:0 blue:0 alpha:1];
        }
    }
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1]];
    passwordField.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
